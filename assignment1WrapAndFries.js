const Order = require("./assignment1Order");

const OrderState = Object.freeze({
    WELCOMING: Symbol("welcoming"),
    SIZE: Symbol("size"),
    TOPPINGS: Symbol("toppings"),
    FRIES: Symbol("fries"),
    SIZEFRIES: Symbol("sizefries"),
    DRINKS: Symbol("drinks")
});

module.exports = class FriesOrder extends Order {
    constructor() {
        super();
        this.stateCur = OrderState.WELCOMING;
        this.sSize = "";
        this.sToppings = "";
        this.sFries = "";
        this.sSizeFries = "";
        this.sDrinks = "";
        this.sAmount = 0;
        this.sItem = "wrap";
    }
    handleInput(sInput) {
        let aReturn = [];
        switch (this.stateCur) {
            case OrderState.WELCOMING:
                this.stateCur = OrderState.SIZE;
                aReturn.push("Welcome to Diamond's WrapAndFries.");
                aReturn.push("What size of wrap would you like?");
                break;
            case OrderState.SIZE:
                this.stateCur = OrderState.TOPPINGS
                this.sSize = sInput;
                if (this.sSize.toLowerCase() == 'small') {
                    this.sAmount += 7;
                }
                else if (this.sSize.toLowerCase() == 'medium') {
                    this.sAmount += 10;
                }
                else if (this.sSize.toLowerCase() == 'large') {
                    this.sAmount += 12;
                }
                else {
                    this.sAmount += 10;
                }
                aReturn.push("What toppings would you like inside wrap?");
                break;
            case OrderState.TOPPINGS:
                this.stateCur = OrderState.FRIES
                this.sToppings = sInput;
                aReturn.push("Would you like to have fries with your wrap?");
                break;
            case OrderState.FRIES:
                this.sFries = sInput;
                if(this.sFries.toLowerCase() == 'no'){
                    this.stateCur = OrderState.DRINKS
                    aReturn.push("Would you like drinks with that?");
                }
                else{
                    this.stateCur = OrderState.SIZEFRIES
                    aReturn.push("What size of fries would you like?");
                }
                break;
            case OrderState.SIZEFRIES:
                this.stateCur = OrderState.DRINKS
                this.sSizeFries = sInput;
                if (this.sSizeFries.toLowerCase() == 'small') {
                    this.sAmount += 3;
                }
                else if (this.sSizeFries.toLowerCase() == 'medium') {
                    this.sAmount += 5;
                }
                else if (this.sSizeFries.toLowerCase() == 'large') {
                    this.sAmount += 7;
                }
                else {
                    this.sAmount += 5;
                }
                aReturn.push("Would you like drinks with that?");
                break;
            case OrderState.DRINKS:
                this.isDone(true);
                if (sInput.toLowerCase() != "no") {
                    this.sDrinks = sInput;
                    this.sAmount += 1;
                }
                aReturn.push("Thank-you for your order of");
                aReturn.push(`${this.sSize} ${this.sItem} with ${this.sToppings}`);
                if (this.sFries.toLowerCase() != "no") {
                    aReturn.push(`${this.sSizeFries} Fries`);
                }
                if (this.sDrinks) {
                    aReturn.push(this.sDrinks);
                }
                let sTax = 0.13 * this.sAmount;
                this.sAmount += sTax;
                aReturn.push(`Total cost of your order is : $${this.sAmount}`);
                let d = new Date();
                d.setMinutes(d.getMinutes() + 20);
                aReturn.push(`Please pick it up at ${d.toTimeString()}`);
                break;
        }
        return aReturn;
    }
}